# Asqatasun-Docker

**Docker** for Asqatasun (`Dockerfile` + `docker-compose.yml`)

[![License : AGPL v3](https://img.shields.io/badge/license-AGPL3-blue.svg)](LICENSE)
[![Build Status](https://gitlab.com/asqatasun/asqatasun-docker/badges/main/pipeline.svg)](https://gitlab.com/asqatasun/asqatasun-docker/pipelines?scope=branches)
[![Code of Conduct](https://img.shields.io/badge/code%20of-conduct-ff69b4.svg?style=flat-square)](CODE_OF_CONDUCT.md)
[![Contributing welcome](https://img.shields.io/badge/contributing-welcome-brightgreen.svg?style=flat-square)](CONTRIBUTING.md)


## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Contact and discussions

- [Asqatasun **Forum**](http://forum.asqatasun.org/)
- [Instant messaging on **Element.io**: +asqatasun:matrix.org](https://app.element.io/#/group/+asqatasun:matrix.org)
- email to `asqatasun AT asqatasun dot org` (only English, French and Klingon is spoken :) )

## Contribute

We would be glad to have you on board! You can help in many ways:

1. Use Asqatasun on your sites !
1. Give us [**feedback** on the forum](http://forum.asqatasun.org)
1. Contribute to Asqatasun
   - [Fill in bug report to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/issues)
   - [**Contribute** to Asqatasun](https://gitlab.com/asqatasun/Asqatasun/-/blob/master/CONTRIBUTING.md) code
1. Contribute to Asqatasun Docker images (current repository)
   - [Fill in bug report](https://gitlab.com/asqatasun/asqatasun-docker/-/issues)
   - [Contribute](https://gitlab.com/asqatasun/asqatasun-docker/-/blob/main/CONTRIBUTING.md) code

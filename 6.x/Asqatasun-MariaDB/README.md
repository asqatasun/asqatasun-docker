# Docker compose for Asqatasun with MariaDB

## Start

```shell
docker compose up
```

## Stop

```shell
docker compose up
```

## Purge

```shell
docker compose down --rmi local ; docker system prune --force
```

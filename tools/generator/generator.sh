#!/bin/bash

set -o errexit

TEMP=$(getopt -o cpt --long default --long help --long asqatasun: --long firefox: --long geckodriver: \
    -n 'javawrap' -- "$@")

    # SC2181: Check exit code directly with e.g. 'if mycmd;', not indirectly with $?.
    # For more information: https://www.shellcheck.net/wiki/SC2181
#    if [[ $? != 0 ]]; then
#        echo "Terminating..." >&2
#        exit 1
#    fi

declare DEFAULT_ASQATASUN_VERSION="5.0.0"
declare DEFAULT_FIREFOX_VERSION="102.8.0esr"
declare DEFAULT_GECKODRIVER_VERSION="0.32.2"

usage() {
    echo ""
    echo " Generate README, Dockerfile, docker-compose.yml and .env.dist files"
    echo " for default or custom versions of Asqatasun, Firefox and Geckodriver."
    echo " --------------------------------------------------------------------"
    echo " usage: tools/generator/generator.sh [OPTIONS]..."
    echo ""
    echo "   --default             Generate files with default versions"
    echo "   --asqatasun   <arg>   Asqatasun   version to be used, by default ${DEFAULT_ASQATASUN_VERSION}"
    echo "   --firefox     <arg>   Firefox     version to be used, by default ${DEFAULT_FIREFOX_VERSION}"
    echo "   --geckodriver <arg>   Geckodriver version to be used, by default ${DEFAULT_GECKODRIVER_VERSION}"
    echo "   --help                Display documentation."
    echo ""
    echo " --------------------------------------------------------------------"
    echo ""
    echo "   ASQATASUN versions"
    echo "   https://gitlab.com/asqatasun/Asqatasun/-/releases"
    echo "   https://forum.asqatasun.org/tag/release"
    echo "   https://download.asqatasun.org/"
    echo ""
    echo "   FIREFOX versions"
    echo "   https://wiki.mozilla.org/Release_Management/Calendar"
    echo "   https://www.mozilla.org/en-US/firefox/releases/"
    echo "   https://download-installer.cdn.mozilla.net/pub/firefox/releases/"
    echo ""
    echo "   GECKODRIVER versions"
    echo "   https://github.com/mozilla/geckodriver/releases"
    echo "   https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html"
    echo ""
    exit 2
}

# Note the quotes around $TEMP: they are essential!
eval set -- "$TEMP"

declare ASQATASUN_VERSION=
declare FIREFOX_VERSION=
while true; do
    case "$1" in
    --help)
        usage
        ;;
    --default)
        break
        ;;
    --asqatasun)
        ASQATASUN_VERSION="$2"
        shift 2
        ;;
    --firefox)
        FIREFOX_VERSION="$2"
        shift 2
        ;;
    --geckodriver)
        GECKODRIVER_VERSION="$2"
        shift 2
        ;;
    --)
        shift
        break
        ;;
    *) break ;;
    esac
done

# Check no mandatory options
if [[ "${ASQATASUN_VERSION}" == "" ]]; then
    ASQATASUN_VERSION="${DEFAULT_ASQATASUN_VERSION}"
fi
if [[ "${FIREFOX_VERSION}" == "" ]]; then
    FIREFOX_VERSION="${DEFAULT_FIREFOX_VERSION}"
fi
if [[ "${GECKODRIVER_VERSION}" == "" ]]; then
    GECKODRIVER_VERSION="${DEFAULT_GECKODRIVER_VERSION}"
fi


#########################################
# Create a new version
#########################################

function create_new-version() {

    FIREFOX_EXTRA_PATH="_Firefox-${FIREFOX_VERSION}"
    if [[ "${FIREFOX_VERSION}" == "${DEFAULT_FIREFOX_VERSION}" ]]; then
        FIREFOX_EXTRA_PATH=""
    fi

    GECKODRIVER_EXTRA_PATH="_Geckodriver-${GECKODRIVER_VERSION}"
    if [[ "${GECKODRIVER_VERSION}" == "${DEFAULT_GECKODRIVER_VERSION}" ]]; then
        GECKODRIVER_EXTRA_PATH=""
    fi

    EXTRA_PATH="${FIREFOX_EXTRA_PATH}${GECKODRIVER_EXTRA_PATH}"
    EXTRA_OS_PATH="${EXTRA_PATH}"
    if [[ "${EXTRA_OS_PATH}" == "" ]]; then
        EXTRA_OS_PATH="_ubuntu-18.04"
    fi

    # Extract version numbers and compute directory path
    IFS='.' read -r -a array <<<"${ASQATASUN_VERSION}" # DEBUG  printf '%s\n' "${array[@]}"
    MAJOR="${array[0]}"
    MINOR="${array[1]}"
    PATCH="${array[2]}"
    DEST_DIR="${MAJOR}.x/${MAJOR}.${MINOR}.y/${ASQATASUN_VERSION}${EXTRA_PATH}"
    SRC_DIR="tools/generator/templates/ZZ_VERSION_ZZ"
    SRC_COMMON_DIR="tools/generator/templates/common"

    # Check if working directory is root directory of Git project
    if [ -d "${SRC_DIR}" ]; then
        pwd
        echo ""
    else
        pwd
        echo "ERROR: working directory must be root of Git project"
        exit 2
    fi

    # Display computed information and backup the directory if exists
    echo "===== Asqatasun ${ASQATASUN_VERSION} ====="
    echo "MAJOR    : ${MAJOR}"
    echo "MINOR    : ${MINOR}"
    echo "PATCH    : ${PATCH}"
    echo "=================="
    echo "SRC_DIR  : ${SRC_DIR}"
    echo "DEST_DIR : ${DEST_DIR}"
    echo "=================="
    if [ -d "${DEST_DIR}" ]; then
        echo "- Backup: ${DEST_DIR}"
        tar -czf "${DEST_DIR}.tar.gz" "${DEST_DIR}"
        echo "- Delete: ${DEST_DIR}"
        rm -rf "${DEST_DIR}"
    fi

    # Create directory and files
    echo "=================="
    mkdir -vp "${DEST_DIR}"

    S1="all-ZZ_VERSION_ZZ_ubuntu-18.04"
    S2="api-ZZ_VERSION_ZZ_ubuntu-18.04"
    S3="webapp-ZZ_VERSION_ZZ_ubuntu-18.04"
    D1="all-${ASQATASUN_VERSION}${EXTRA_OS_PATH}"
    D2="api-${ASQATASUN_VERSION}${EXTRA_OS_PATH}"
    D3="webapp-${ASQATASUN_VERSION}${EXTRA_OS_PATH}"
    cp -fr "${SRC_DIR}/${S1}" "${DEST_DIR}/${D1}"
    cp -fr "${SRC_DIR}/${S2}" "${DEST_DIR}/${D2}"
    cp -fr "${SRC_DIR}/${S3}" "${DEST_DIR}/${D3}"
    cp -fr "${SRC_COMMON_DIR}/bin" "${DEST_DIR}/${D1}"
    cp -fr "${SRC_COMMON_DIR}/bin" "${DEST_DIR}/${D2}"
    cp -fr "${SRC_COMMON_DIR}/bin" "${DEST_DIR}/${D3}"

    cd "${DEST_DIR}/${D1}"
    find . -type f -exec sed -i "s/ZZ-version-ZZ/${ASQATASUN_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-firefox-version-ZZ/${FIREFOX_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-geckodriver-version-ZZ/${GECKODRIVER_VERSION}/g" {} \;
    cd "../${D2}"
    find . -type f -exec sed -i "s/ZZ-version-ZZ/${ASQATASUN_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-firefox-version-ZZ/${FIREFOX_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-geckodriver-version-ZZ/${GECKODRIVER_VERSION}/g" {} \;
    cd "../${D3}"
    find . -type f -exec sed -i "s/ZZ-version-ZZ/${ASQATASUN_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-firefox-version-ZZ/${FIREFOX_VERSION}/g" {} \;
    find . -type f -exec sed -i "s/ZZ-geckodriver-version-ZZ/${GECKODRIVER_VERSION}/g" {} \;

    echo "=================="
    echo "Firefox    : ${FIREFOX_VERSION}"
    echo "Geckodriver: ${GECKODRIVER_VERSION}"
    echo "Asqatasun: ${ASQATASUN_VERSION}"
}

##########################################
# Main tasks
##########################################
create_new-version

exit 0

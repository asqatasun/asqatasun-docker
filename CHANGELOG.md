# CHANGELOG

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

----------------

## 2.2.0 - 2021-10-26

### Added

- Add Asqatasun **5-SNAPSHOT**

----------------

## 2.1.0 - 2021-09-26

### Added

- Improve Docker generator: allow to use default or custom versions of Asqatasun, Firefox and Geckodriver
- Add Asqatasun **5.0.0-rc.1** with Firefox **78.14 ESR**
- Add Asqatasun **5.0.0-rc.1** with Firefox **91.1 ESR**
- Add Asqatasun **5.0.0-rc.1** with Firefox **91.1 ESR** + Geckodriver **0.30**

----------------

## 2.0.1 - 2020-12-06

### Fixed

- lighten Docker images (remove `.tar.*` files and clean up `var/lib/apt/lists/`)

----------------

## 2.0.0 - 2020-12-06

### Changed

- change webapp default port (`8080` instead off `8982`)
- change API server default port (`8081` instead off `8986`)
- change database server default port (`3306` instead off `9924`)

### Removed

- remove Asqatasun **5.0.0-alpha.2**

----------------

## 1.4.0 - 2020-11-16

### Added

- Add Asqatasun **5.0.0-rc.1**

----------------

## 1.3.0 - 2020-11-12

### Added

- Add Asqatasun **5.0.0-alpha2**
- CI - Add Markdown files linter (manuel task)

### Removed

- Temporary remove of SNAPSHOT version

----------------

## 1.2.0 - 2020-11-08

### Added

- CI - Add relative links checker for .md file
- CI - Add external links checker for .md file
- CI - Add DockerFile linter (Hadolint)
- CI - Add docker-compose.yml linter (via "docker-compose config")
- CI - Add .env linter (dotenv-linter/dotenv-linter)
- CI - Add shell scripts linter (shellcheck)
- CI - Add yaml linter (yamllint)

----------------

## 1.1.0 - 2020-11-07

### Added

- Add Asqatasun 5.0.0-SNAPSHOT api
- Add Asqatasun 5.0.0-SNAPSHOT all (api + webapp)

### Fixed

- doc: add rebuild process for SNAPSHOT
- doc: improve README files

----------------

## 1.0.1

### Fixed

- move 5.0.0-SNAPSHOT directory to right place

----------------

## 1.0.0

### Added

- add Asqatasun 5.0.0-SNAPSHOT webapp

----------------

## Template

```markdown
## Unreleased

### Added

### Changed

### Deprecated

### Removed

### Fixed

### Security

```

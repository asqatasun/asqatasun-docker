# Build with Docker compose

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy [`.env.dist`](.env.dist) file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Software versions

* Asqatasun **5.0.0-rc.1**
* Geckodriver **0.30.0**
* Firefox **102.8.0esr**

## Start Asqatasun with Docker-Compose

Build and launch:

```shell
docker-compose up --build
```

## Play with Asqatasun webapp

* In your browser, go to `http://127.0.0.1:8080/`
* Use this user and this password to log in:
  * `admin@asqatasun.org`
  * `myAsqaPassword`

## Stop Asqatasun

```shell
docker-compose down
```

# How to publish image to Docker hub

```shell script
docker build -t asqatasun/asqatasun:5.0.0-rc.1 .    # build
docker-compose up --build                           # verify all is good
docker-compose down                                 # tear down containers
docker login                                        # Log in Docker Hub
docker push asqatasun/asqatasun:5.0.0-rc.1          # Push to Docker Hub
```

Repeat same operations with `latest` tag


```shell script
docker build -t asqatasun/asqatasun:latest .        # build
docker-compose up --build                           # verify all is good
docker-compose down                                 # tear down containers
docker login                                        # Log in Docker Hub
docker push asqatasun/asqatasun:latest              # Push to Docker Hub
```

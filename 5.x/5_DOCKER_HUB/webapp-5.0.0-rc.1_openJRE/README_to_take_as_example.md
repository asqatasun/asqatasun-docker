# READMEs of Docker images to take as example

## Famous images taken as example for their README

* https://hub.docker.com/_/wordpress
* https://hub.docker.com/_/matomo
* https://hub.docker.com/r/linuxserver/duplicati
* https://hub.docker.com/r/bitnami/prometheus
* https://hub.docker.com/r/grafana/grafana
* https://hub.docker.com/_/joomla

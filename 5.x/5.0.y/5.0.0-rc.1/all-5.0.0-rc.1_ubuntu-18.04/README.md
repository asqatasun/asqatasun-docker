# Launch Asqatasun `5.0.0-rc.1` API + Webapp

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy [`.env.dist`](.env.dist) file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.

## Software versions

- Asqatasun **5.0.0-rc.1**
- Geckodriver **0.30.0**
- Firefox **102.8.0esr**

## Launch Asqatasun webapp with Docker-Compose

- Build docker images
- Launch Asqatasun API
- Launch Asqatasun webapp

### Option 1: uses same database, if it already exists

```shell
# build Docker image + launch Asqatasun and database (uses same database, if it already exists)
docker-compose up  --build
```

### Option 2: uses a new database

```shell
# build Docker image + launch Asqatasun and a new database
docker-compose rm -fsv
docker-compose up  --build
```

## Play with Asqatasun webapp

- In your browser, go to `http://127.0.0.1:8080/`
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

## Play with Asqatasun API

- In your browser: `http://127.0.0.1:8081/`  (API documentation and **Swagger** playground)
- Use this user and this password to log in:
  - `admin@asqatasun.org`
  - `myAsqaPassword`

You can refer to [Asqatasun API documentation](https://doc.asqatasun.org/v5/en/Developer/API/)
for full usage and tips on how to use it.

### Launch an audit via API

Launch an audit via the API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"
API_URL="${API_PREFIX_URL}/api/v1/audit/run"

PROJECT_ID="1"
REFERENTIAL="RGAA_4_0"
LEVEL="AA"
URL_TO_AUDIT=https://www.wikidata.org
curl -X POST \
     "${API_URL}"                                               \
     -H  "accept: */*"                                          \
     -H  "Content-Type: application/json"                       \
     -d "{                                                      \
            \"urls\": [    \"${URL_TO_AUDIT}\"  ],              \
                           \"referential\": \"${REFERENTIAL}\", \
                           \"level\": \"${LEVEL}\",             \
                           \"contractId\": ${PROJECT_ID},       \
                           \"tags\": []                         \
         }"
```

#### API returns the audit ID

API returns the ID of the audit that has just been launched.

```shell
7 # Audit ID
```

### Display the result of an audit via API

Display the result of an audit via API using the following command lines:

```shell
ASQA_USER="admin%40asqatasun.org"
ASQA_PASSWORD="myAsqaPassword"
API_PREFIX_URL="http://${ASQA_USER}:${ASQA_PASSWORD}@localhost:8081"

AUDIT_ID="7"
API_URL="${API_PREFIX_URL}/api/v1/audit/${AUDIT_ID}"
curl -X GET "${API_URL}" -H  "accept: */*"
```

#### API returns JSON data

API returns a JSON content which contains information of requested audit (status, results, ...).

```json
{
   "referential" : "RGAA_4_0",
   "subject" : {
      "grade" : "E",
      "type" : "PAGE",
      "repartitionBySolutionType" : [
         {
            "type" : "PASSED",
            "number" : 8
         },
         {
            "number" : 3,
            "type" : "FAILED"
         },
         {
            "number" : 29,
            "type" : "NEED_MORE_INFO"
         },
         {
            "number" : 71,
            "type" : "NOT_APPLICABLE"
         },
         {
            "type" : "NOT_TESTED",
            "number" : 146
         }
      ],
      "id" : 1,
      "url" : "https://www.wikidata.org/wiki/Wikidata:Main_Page",
      "mark" : 72.73,
      "nbOfPages" : 1
   },
   "id" : 1,
   "status" : "COMPLETED",
   "date" : "2020-11-07T14:08:36.000+0000",
   "referentialLevel" : "LEVEL_2",
   "tags" : []
}
```

# Asqatasun `5.0.0-rc.1`

`Dockerfile` and `docker-compose-.yml` files that are available:

- [Asqatasun `5.0.0-rc.1` **API**](api-5.0.0-rc.1_ubuntu-18.04)
- [Asqatasun `5.0.0-rc.1` **Webapp**](webapp-5.0.0-rc.1_ubuntu-18.04)
- [Asqatasun `5.0.0-rc.1` **API** + **Webapp**](all-5.0.0-rc.1_ubuntu-18.04)

> ⚠️ Created for testing purpose only, no security has been made for production. ⚠️

## Prerequisites

- [Docker](https://docs.docker.com/engine/install/) `19.03.0` (at least) is required
- [Docker Compose](https://docs.docker.com/compose/install/) `1.27.0` (at least) is required

## Ports, URL and credentials (user/password)

| Service  | Port | URL                                     | User                         | Password                        |
|----------|------|-----------------------------------------|------------------------------|---------------------------------|
| Database | 3306 | `jdbc:mysql://localhost:3306/asqatasun` | `asqatasunDatabaseUserLogin` | `asqatasunDatabaseUserP4ssword` |
| API      | 8081 | `http://localhost:8081`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |
| Webapp   | 8080 | `http://localhost:8080`                 | `admin@asqatasun.org`        | `myAsqaPassword`                |

Tip:
if you copy `.env.dist` file to `.env` file,
you can change **port** numbers, **IP** adresses and **database** credentials.


## Software versions

- Asqatasun **5.0.0-rc.1**
- Geckodriver **0.30.0**
- Firefox **102.8.0esr**
